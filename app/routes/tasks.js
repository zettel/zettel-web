import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
	model() {
		return [{
			id: '1bcdec07-3f3a-4b79-b4fb-0b152d798acf',
			name: 'Butter'
		},{
			id: 'fd68be09-0798-4aba-8948-a744cd5ff6ee',
			name: 'Schokolade'
		},{
			id: '44612528-cab1-4140-b15c-18bb6f6edbcc',
			name: 'Waschmittel'
		},
		];
	}  
});
