import Ember from 'ember';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';

export default Ember.Route.extend(ApplicationRouteMixin, {
  session: Ember.inject.service('session'),

  model(params) {
    console.log("login route")
    const { token } = params;
    if (token) {
      console.log("token " + token)
      this.get('session').authenticate('authenticator:token', token);
      this.transitionTo('tasks');
    }
  }
});
