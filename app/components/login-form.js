import Ember from 'ember';

export default Ember.Component.extend({
  session: Ember.inject.service('session'),
  isWaiting: false,

  actions: {
    authenticate() {
      var self = this
      let { emailAddress } = this.getProperties('emailAddress');
      console.log("Authenticate Email Address: " + emailAddress)
      this.get('session').authenticate('authenticator:custom', emailAddress, function() {
          console.log("Wait for token...");
          self.set('isWaiting', true)
      }).catch((reason) => {
        self.set('isWaiting', false);
        this.set('errorMessage', reason);
      });
    }
  },

});
