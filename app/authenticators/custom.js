// app/authenticators/custom.js
import Ember from 'ember';
import Base from 'ember-simple-auth/authenticators/base';

export default Base.extend({

  tokenEndpoint: 'http://api.test.authlink.net:8080/authentications',
  pollingCount: 0,

  restore: function(data) {
      return new Ember.RSVP.Promise(function(resolve, reject) {
          console.log("restore...")
          if (!Ember.isEmpty(data.token)) {
              resolve(data);
          } else {
              reject();
          }
      });
  },

  authenticate: function(emailAddress, waitForTokenCallBack) {
      var self = this;
      return new Ember.RSVP.Promise((resolve, reject) => {

          // init a new authentication request
          Ember.$.ajax({
              url: this.tokenEndpoint,
              type: 'POST',
              data: JSON.stringify({
                  user: emailAddress,
                  client: 'zettel.io',
                  redirectUrl: 'http://localhost:4200/tasks'
              }),
              contentType: 'application/json;charset=utf-8',
              dataType: 'json'
          }).then(function(response) {
              Ember.run(function() {

                // when authentication was created (201) a token link is returned
                // the user should now go to the app an approve the authentication request

                waitForTokenCallBack();

                let tokenLink = response._links.token.href;
                console.log("Token Link: " + tokenLink);

                self.pollForToken(self, tokenLink, emailAddress, resolve, reject);

//                Ember.$.ajax({
//                  url: tokenLink
//                }).then(function(response) {
//                  let status = response.status
//                  let accessToken = response.accessToken
//                  console.log("Got status " + status  + " and token " + accessToken)
//                  if(accessToken) {
//                    console.log("Resolving access token")
//                    resolve({
//                      access_token: accessToken,
//                      token_type: 'bearer',
//                      username: emailAddress
//                    });
//                  }
//                }, function(xhr, status, error) {
//                  reject(response);
//                });

              });
          }, function(xhr, status, error) {
              var response = status;
              Ember.run(function() {
                  reject(response);
              });
          });
      });
  },

  invalidate: function() {
      console.log('invalidate...');
      return Ember.RSVP.resolve();
  },

  pollForToken(self, tokenLink, emailAddress, resolve, reject) {
    self.pollingCount = self.pollingCount + 1;
    Ember.$.ajax({
      url: tokenLink
    }).then(function(response) {
      let status = response.status
      let accessToken = response.accessToken
      console.log("Got status " + status  + " and token " + accessToken)
      if(accessToken) {
        console.log("Resolving access token")
        resolve({
          access_token: accessToken,
          token_type: 'bearer',
          username: emailAddress
        });
      } else {
        if(self.pollingCount > 5){
          reject("Authentication request has not been approved. Please try again.");
        }

        self.pollForToken(self, tokenLink, emailAddress, resolve, reject);
      }
    }, function(xhr, status, error) {
      reject(response);
    });
  },

  getTokenData: function(token) {
    const payload = token.split('.')[1];
    const tokenData = decodeURIComponent(window.escape(atob(payload)));

    try {
      return JSON.parse(tokenData);
    } catch (e) {
      return tokenData;
    }
  }
});
