import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('waiting-for-token', 'Integration | Component | waiting for token', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{waiting-for-token}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#waiting-for-token}}
      template block text
    {{/waiting-for-token}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
