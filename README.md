# zettel.io

This application is related to my Master Thesis to demonstrate the authlink authentication scheme as a proof of concept.

## Deployment
The application is deployed to zettel.io

## Installation

* `git clone <repository-url>` this repository
* `cd zettel-web`
* `npm install`

## Running

* `ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).

